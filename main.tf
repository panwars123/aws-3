provider "aws" {
  region = "us-east-1"
}



# aws_vpc.main:
resource "aws_vpc" "main" {
#    arn                              = "arn:aws:ec2:us-east-1:732026571143:vpc/vpc-0c1390a1efab6667d"
    assign_generated_ipv6_cidr_block = false
    cidr_block                       = "10.0.0.0/16"
#    default_network_acl_id           = "acl-0ad825fbcc238c87f"
#    default_route_table_id           = "rtb-02c736a923358f2f6"
#    default_security_group_id        = "sg-0d99c4d80a3c1d15d"
#    dhcp_options_id                  = "dopt-76f4b80c"
    enable_classiclink               = false
    enable_classiclink_dns_support   = false
    enable_dns_hostnames             = false
    enable_dns_support               = true
#    id                               = "vpc-0c1390a1efab6667d"
    instance_tenancy                 = "default"
#    main_route_table_id              = "rtb-02c736a923358f2f6"
#    owner_id                         = "732026571143"
    tags                             = {
        "Name" = "main"
    }
}
